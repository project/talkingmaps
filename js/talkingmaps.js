function edit_story_modal(pageURL, modalTitle, w="80%", h="80%", autosave=false){
  Drupal.ajax({
    url: pageURL,
    success: function(response) {
    var $paDialogContents
    for (var key in response) {
    // skip loop if the property is from prototype
    if (!response.hasOwnProperty(key)) continue;
    var obj = response[key];
    for (var prop in obj) {
    // skip loop if the property is from prototype
    if(!obj.hasOwnProperty(prop)) continue;
    //console.log(prop + " = " + obj[prop]);
    if(prop == "data"){
    console.log(prop + " = " + obj[prop]);
    $paDialogContents = jQuery('<div>' + response[key].data + '</div>').appendTo('body');
    }
    }
    }
    var paDialog = Drupal.dialog($paDialogContents, {title: modalTitle, dialogClass: "story-edit-dialog", width: w, height: h});
    paDialog.showModal();
    console.log(paDialog);
    Drupal.attachBehaviors(jQuery(".story-edit-dialog"));

    if(autosave){
      //saveDB();
      //console.log(jQuery('#tm_json').html());
      //var json = $('#story_iframe').contents().find('#tm_json');

      //get talkingmaps global variable data
      var iframeID = document.getElementById("story_iframe");
      //focus the IFRAME element
      $(iframeID).focus();
      //use JQuery to find the control in the IFRAME and set focus
      $(iframeID).contents().find("#button_save_changes").click();

      var tm_json = document.getElementById("story_iframe").contentWindow.GlobalTalkingStory;
      console.log('HEEEEERE');
      console.log(tm_json);

      $.when(tm_json).done(function(){
        jQuery('textarea[name="field_tm_json[0][value]"]').val(escape(JSON.stringify(tm_json,null,2)));
      }).done(function(){
        jQuery('input[data-drupal-selector="edit-submit"]')[0].click();
      });
    }else{

    }
   }
  }).execute();
}

function loadFromDrupal(){

  $('#story_iframe').ready(function(){

    $('#loader').show();
    $('#empty_loading_background').show();

//    var unescaped_json = unescape('{{ content.field_tm_json.0 }}');
  var unescaped_json = unescape($('#field_tm_json').text());
    if (unescaped_json.length > 0 && unescaped_json != 'null'){
      var drupal_tm_json = JSON.parse(unescaped_json);
      var story_id = Object.keys(drupal_tm_json)[0];
      console.log(story_id);
      console.log(drupal_tm_json);
      $.when(drupal_tm_json).done(function(){
          console.log(story_id);
          console.log(drupal_tm_json);
          var iframeID = document.getElementById("story_iframe");
          //focus the IFRAME element
          $(iframeID).focus();

          setTimeout(function(){
            document.getElementById("story_iframe").contentWindow.GlobalContents = drupal_tm_json[story_id].GlobalContents;
            document.getElementById("story_iframe").contentWindow.GlobalOptions = drupal_tm_json[story_id].GlobalOptions;
            document.getElementById("story_iframe").contentWindow.GlobalMaps = drupal_tm_json[story_id].GlobalMaps;
            //document.getElementById("story_iframe").contentWindow.initialize_with_first_slide();
            $('#loader').hide();
            $('#empty_loading_background').fadeOut(2000);
          },1000);
      });
    }
  });
}

function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',10):f()}
// use like
r(function(){
    $=jQuery;
//    var edit_link = "/node/{{ node.id }}/edit";
    var edit_link = "/"+drupalSettings.path.currentPath+"/edit";
    loadFromDrupal();

    $('body').addClass('overflow_hidden');

    //enable esri edit modal
    //$('#bd-storymap-modal-sm').click(function(){ display_esri_edit_modal() });

    //..check if administrative menu exists
    if(document.getElementById("toolbar-administration") !== null){
      $('#story_iframe').addClass('story_iframe_class_admin');
    }

    $('nav.main ul, nav.main .toolbar-icons-right, nav.main .block-superfish').fadeOut('1500',function(){
      $('nav.main').addClass('nav-minified');
    });


    //..checking if it is an ESRI storymap
    var esri_appid = $('#field_tm_esri').text();
      // checking templates if a template is defined >> put the default one under /storymap/ and other templates for example in /storymap/casscade/ 
      var template_path = "/storymap/index.html?appid="+esri_appid;
      var esri_template = esri_appid.split('/');
      if (esri_template.length > 1 ){
        template_path = "/storymap/"+esri_template[0]+"/index.html?appid="+esri_template[1];
      }

    var newesri_appid = $('#field_tm_newesri').text();

    //.. OLD ESRI STORYMAPs
    if (esri_appid != ''){

      var metalink = "<button id='metalink_button' class='btn btn-link'><i class='fas fa-receipt'></i></button>";
      var editlink = "<a href='"+template_path+"&edit=true' target='_blank'><button id='edit_talkingmap' class='btn btn-link' data-toggle='tooltip' title='Edit this ESRI Storymap'><i class='fas fa-edit'></i></button></a>";
      var save_link = "";
      var home_link = "<a href='/'><button id='home_button' class='btn btn-link' data-toggle='tooltip' title='Go Home :)'><i class='fas fa-home'></i></button></a>";

      console.log('ESRI story Loading:' + esri_appid);
      $('#story_iframe').attr('src',template_path);

    //.. NEW ESRI STORYMAPs
    }else if(newesri_appid != ''){

      var metalink = "<button id='metalink_button' class='btn btn-link'><i class='fas fa-receipt'></i></button>";
      var editlink = "<a href='https://storymaps.arcgis.com/stories/"+newesri_appid+"/edit' target='_blank'><button id='edit_talkingmap' class='btn btn-link' data-toggle='tooltip' title='Edit this ESRI Storymap'><i class='fas fa-edit'></i></button></a>";
      var save_link = "";
      var home_link = "<a href='/'><button id='home_button' class='btn btn-link' data-toggle='tooltip' title='Go Home :)'><i class='fas fa-home'></i></button></a>";

      console.log('ESRI NEW story Loading:' + newesri_appid);
      $('#story_iframe').attr('src','https://storymaps.arcgis.com/stories/'+newesri_appid);

    //.. TALKINGMAPS
    }else{

      //var metalink = "<a href='{{url('<front>')}}node/{{ node.id }}/edit' class='use-ajax add-indicator fancy_login_show_popup' data-dialog-type='modal'><i class='fas fa-receipt'></i></a>";
      var metalink = "<button id='metalink_button' class='btn btn-link'><i class='fas fa-receipt'></i></button>";
      var editlink = "<button id='edit_talkingmap' class='btn btn-link' data-toggle='tooltip' title='Edit this TalkingMap'><i class='fas fa-edit'></i></button>";
      var save_link = "<button id='save_talkingmap' class='btn btn-link' data-toggle='tooltip' title='Save the Story'><i class='fas fa-save'></i></button>";
      var home_link = "<a href='/'><button id='home_button' class='btn btn-link' data-toggle='tooltip' title='Go Home :)'><i class='fas fa-home'></i></button></a>";

      //TODO!!! parametrize all main div containers so for Drupal or other CMS the div to add the icons can be set in a configuration file.
    }

    if (drupalSettings.user.uid !=0){
      $('#floating_toolbar').append(home_link + "\
      <div class='btn btn-link' data-toggle='tooltip' title='Edit Story Metadata'>" + metalink + "</div>" + editlink + save_link);
    }else{
      $('#floating_toolbar').append(home_link + editlink);
    }

    //$('[data-toggle="tooltip"]').tooltip();

    $('#metalink_button').on('click',function(){
          edit_story_modal(edit_link, 'Edit BIOPAMA Story Metadata');
    });

    $('#save_talkingmap').on('click',function(){
      var openmodal = edit_story_modal(edit_link, 'Saving Your Story','80%','80%',autosave=true);
    });

    if (esri_appid == '' && newesri_appid==''){
      $('#edit_talkingmap').on('click',function(){

        $('#floating_toolbar').focus();
        //todo check if already edit then go to preview and viceversa
        $('#story_iframe').remove();
      //      var iframe = '<iframe id="story_iframe" class="story_iframe_class" src="/libraries/talkingmaps/?id={{ content.field_tm_application_id.0 }}&edit=true" style="width:100%;"></iframe>';
        var iframe = '<iframe id="story_iframe" class="story_iframe_class" src="/libraries/talkingmaps/?id='+$('#field_tm_application_id').text()+'&edit=true" style="width:100%;"></iframe>';
        $('#iframe_wrapper').append(iframe);
        setTimeout(function(){
          document.getElementById("story_iframe").location.reload();
        },1000);

        $('#floating_toolbar').focus();
        if(document.getElementById("toolbar-administration") !== null){
          $('#story_iframe').addClass('story_iframe_class_admin');
        }
        loadFromDrupal();

        setTimeout(function(){
          //get talkingmaps global variable data
          var iframeID = document.getElementById("story_iframe");
          //focus the IFRAME element
          $(iframeID).focus();
          document.getElementById("story_iframe").contentWindow.initialize_with_first_slide();
          //hide talkingmaps buttons to use the Drupal ones
          $(iframeID).contents().find("#button_save_changes").hide();

          },2000);

      });
    }

    Drupal.attachBehaviors(jQuery("#story_iframe"));

});
